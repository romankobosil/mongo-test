# sense.ai.tion system - mongo-test
# ----------------------------------------------
# 
# (c) 2020 by sense.ai.tion GmbH, <Roman.Kobosil@senseaition.com>
#

FROM golang:1.17-alpine

RUN apk update && apk add ca-certificates git && rm -rf /var/cache/apk/*
RUN update-ca-certificates

WORKDIR /home/
COPY ./ /home/

RUN cd /home && ls
RUN env GIT_TERMINAL_PROMPT=1 GOSUMDB=off GOPRIVATE=git.kobosil.me CGO_ENABLED=0 go build -o /bin/test ./main.go

# definition of container
FROM alpine:latest
LABEL author="sense.ai.tion"
LABEL maintainer="<Roman.Kobosil@senseaition.com"
LABEL version="1.0"
LABEL description="This service module is part of the sense.ai.tion-AI-engine and generates psychological classifications for texts"

# installation of necessary certificates for ssl communication
RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=0 /bin/test /bin/test
COPY ./ca-certificate.crt /root/ca-certificate.crt

# start program/process
STOPSIGNAL SIGTERM
EXPOSE ${SMPORT}
ENTRYPOINT /bin/test

