SHELL := /bin/bash

docker:
	@go mod vendor
	@docker -l fatal image build --compress --tag "registry.gitlab.com/romankobosil/mongo-test:latest" .
	@docker -l fatal image push "registry.gitlab.com/romankobosil/mongo-test:latest"
