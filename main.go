package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Data struct {
	ID      string `bson:"_id"`
	Counter int
	Bytes   []byte
}

func main() {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://doadmin:79d2vg53pj4u60Bk@private-db-test-abba6b3f.mongo.ondigitalocean.com/?authSource=admin&replicaSet=db-test&tls=true&tlsCAFile=/root/ca-certificate.crt"))

	if err != nil {
		log.Fatalf("setting up client: %s", err)
	}

	if err = client.Connect(context.Background()); err != nil {
		log.Fatalf("connecting with client: %s", err)
	}

	c := client.Database("test").Collection("mongosimple")
	if _, err := c.DeleteMany(context.Background(), bson.M{}); err != nil {
		log.Fatalf("cleaning collection 'mongosimple': %s", err)
	}

	log.Print("insert data")
	for i := 0; i < 1000; i++ {
		c.InsertOne(context.Background(), &Data{Counter: i})
	}
	log.Print("finished insert data")
}
